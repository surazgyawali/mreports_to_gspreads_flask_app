# %%
from posixpath import join
import gspread
from pdf.kvmap import keysMappingGPR, keysMappingHPR, keysMappingLBR

CRED_JSON_PATH = 'sheet_creds_file.json'
ALL_KVMAPS = {**keysMappingGPR, **keysMappingHPR, **keysMappingLBR}

# gc = gspread.service_account(filename=CRED_JSON_PATH)
gc = gspread.service_account()

sheet = gc.open_by_key('1JpN9VMcmNA4F-1Hjpifs_RDC6A-v87JhNDFpkSYChCs')
ws = sheet.get_worksheet(0)
headers = ws.row_values(1)

def get_matched_kvpairs(matched_rows):
    matched_kv_list = dict.fromkeys(matched_rows)
    for row_num in matched_rows:
        values = ws.row_values(row_num)
        matched_kv_list[row_num] = dict(zip(headers,values))
    return matched_kv_list

def split_name(full_name):
    first,middle,last = None,None,None
    splitted = full_name.split()
    if len(splitted) == 2:
        first,last = splitted
    if len(splitted) == 3:
        first,middle,last = splitted
    return first,middle,last

def get_existing_row(name,ws=ws):
    f,m,l= split_name(name.lower())
    existing_fnames = ws.col_values(headers.index('First Name') + 1)
    existing_lnames = ws.col_values(headers.index('Last Name') + 1)
    existing_names = [f"{name[0]} {name[1]}" for name in zip(existing_fnames,existing_lnames)]
    matches = []
    for idx,cname in enumerate(existing_names,1):
        if  f.strip() in cname.lower():
            if l.strip() in cname.lower():
                if m:
                    if m.strip() in cname.lower():
                        matches.append(idx)
                        continue
                matches.append(idx)
    return matches

# %%

def update_all(jObj,ws=ws):

    dataObj = dict.fromkeys(headers)
    all_data = []
    new_columns = set()

    name = jObj.get('name')
    if name:
        fname,mname,lname = split_name(name)
        jObj["lname"] = lname.title()
        jObj["fname"] = fname.title()

    matched_rows = []
    dob_matches = []
    jObj_dob = jObj.get('dob')
    if jObj_dob:
        try:
            matched_rows = get_existing_row(name)
        except Exception as e:
            print(f"Something went wrong while checking matching rows, cause: {e}")
            # print("No existing rows matched.")


    if len(matched_rows) > 0:
        print(f"Matched Rows:{matched_rows}")
        matched_pairs = get_matched_kvpairs(matched_rows)
        dob_matches = {k:item for k,item in matched_pairs.items() if item.get(ALL_KVMAPS.get('dob')) == jObj_dob}
    else:
        print(f"No name match appending a new row.")

    is_update = False
    if len(dob_matches) > 0:
        is_update = True
        row_num = list(dob_matches)[-1]
        dataObj = dob_matches.get(row_num)

    for header in headers:
        curr_item = [header,None]
        for k,v in jObj.items():
            dataObjKey = ALL_KVMAPS.get(k)
            if not dataObjKey:
                continue

            if not isinstance(dataObjKey,list):
                dataObjKeys = [dataObjKey]
            else:
                dataObjKeys = dataObjKey

            for dataObjKey in dataObjKeys:
                if dataObjKey not in headers:
                    new_columns.add(dataObjKey)
                    continue

                if dataObjKey == header:
                    curr_item = [header,v]
                    dataObj[dataObjKey] = v

        all_data.append(curr_item)

    values = [v for _,v in all_data]
    if is_update:
        print("updating existing")
        ws.update(f'A{row_num}',[values])
        print(row_num)
        print(values)
    else:
        ws.append_row(values)

    if new_columns:
        print("\nCreate this columns in sheet to update these values as well:")
        print("\n".join(new_columns))

    # print("\nSheet successfully updated with following values:")
    # print("\n".join([f"{k} : {v}" for k,v in dataObj.items() if v]))
    return dataObj,all_data,values
