import pathlib
import re

import pdftotext

GPR_PAGE_LBLS = (
    "PERSONAL DETAILS",
    "MOOD & BEHAVIOUR REPORT",
    "CARDIOVASCULAR REPORT",
    "METHYLATION REPORT",
    "METABOLISM AND DIET REPORT",
    "DETOXIFICATION AND ANTI-OXIDATION REPORT",
)

GPR_KEYS = [
    "name",
    "barcode_no",
    "COMT_Rs4680",
    "DRD2_Rs1800497",
    "ADRA2B_Indel",
    "5-HTTLPR_Indel",
    "BDNF_Rs6265",
    "9P21_Rs10757278",
    "9P21_Rs10757274",
    "APOE_Rs429358_Rs7412",
    "NOS3_Rs1799983",
    "ACE_Rs4343",
    "SLCO1B1_Rs4149056",
    "FUT2_Rs601338",
    "MTHFR_Rs1801133",
    "SHMT1_Rs1979277",
    "MTR_Rs1805087",
    "MTRR_Rs1801394",
    "MTHFR_Rs1801131",
    "FTO_Rs9939609",
    "MC4R_Rs17782313",
    "APOA2_Rs5082",
    "UCP1_1800592",
    "TCF7L2_Rs12255372",
    "AMY1_Rs4244372",
    "MCM6_Rs4988235",
    "BCMO1_Rs11645428",
    "SLC23A1_Rs33972313",
    "CYP2R1_Rs10741657",
    "GC_Rs4588",
    "CYP1A2_Rs762551",
    "GSTT1_CNV",
    "GSTM1_CNV",
    "GSTP1_Rs1695",
    "SOD2_Rs4880",
]


GPR_VALUE_PTRN = re.compile(r"[A-Z]/[A-Z]")
GPR_VALUE_PTRN2 = re.compile(r"\d/\d")


def is_data_page(page, PAGE_LBLS):
    return any(word in page[:200] for word in GPR_PAGE_LBLS)


def get_gstt(lines):
    clean_gstt_lines = [line[7:36].strip() for line in lines]
    value            = "".join(clean_gstt_lines).replace("✔", " ")
    return value


def get_dataObj_GPR(pdf_path):
    pdf_path = pathlib.Path(pdf_path)
    with open(pdf_path, "rb") as f:
        pdf = pdftotext.PDF(f, raw=False)

    data = dict.fromkeys(GPR_KEYS)
    data_lines = []
    data_count = 0
    for page_no, page in enumerate(pdf):
        if not is_data_page(page, GPR_PAGE_LBLS):
            continue

        lines = page.splitlines()
        for idx, line in enumerate(lines):
            matchObj = None
            if "NAME:" in line[:20]:
                curr_line_clean = line.replace("NAME:", "").replace("_", "").strip()
                if len(curr_line_clean) > 4:
                    data["name"] = curr_line_clean
                    data_count += 1
                else:
                    data["name"] = lines[idx - 1].strip()
                    data_count += 1

            if "BARCODE NO:" in line[:30]:
                data["barcode_no"] = lines[idx - 1].strip()
                data_count += 1

            if "✔" in line:
                matchObj = re.findall(GPR_VALUE_PTRN, line) or re.findall(GPR_VALUE_PTRN2,line)
                if not matchObj:
                    matchObj1 = re.findall(GPR_VALUE_PTRN, lines[idx-1]) or re.findall(GPR_VALUE_PTRN2,lines[idx-1])
                    matchObj2 = re.findall(GPR_VALUE_PTRN, lines[idx+1]) or re.findall(GPR_VALUE_PTRN2,lines[idx+1])
                    matchObj  = matchObj1 or matchObj2

                if matchObj:
                    data[GPR_KEYS[data_count]] = matchObj[0].strip()
                else:
                    data[GPR_KEYS[data_count]] = line.replace("✔", "").strip()
                    if GPR_KEYS[data_count] == "GSTT1_CNV":
                        gstt_lines = lines[idx - 1 : idx + 2]
                        data[GPR_KEYS[data_count]] = get_gstt(gstt_lines)
                    if GPR_KEYS[data_count] == "GSTM1_CNV":
                        gstt_lines = lines[idx - 1 : idx + 2]
                        data[GPR_KEYS[data_count]] = get_gstt(gstt_lines)
                data_count += 1

    return data

def get_dataObj_HPR(pdf_path):
    pdf_path = pathlib.Path(pdf_path)
    with open(pdf_path, "rb") as f:
        pdf = pdftotext.PDF(f, raw=False)

    data = {}
    start_parsing = False
    for page_no,page in enumerate(pdf):
        lines = page.splitlines()
        if page_no == 1 or 'GENE' in page and '/' in page and 'FAMILY' in page and 'RESULT' in page:
            for idx,line in enumerate(lines):
                if "NAME:" in line[:20]:
                    curr_line_clean = line.replace('NAME:','').replace('_','').strip()
                    if len(curr_line_clean) > 4:
                        data['name'] = curr_line_clean
                    else:
                        data['name'] = lines[idx-1].strip()

                if "BARCODE NO:" in line[:30]:
                    line = line.replace('BARCODE NO:','').replace('_','').strip()
                    data['barcode_no'] = line.strip()


                if "REPORT DATE:" in line[:30]:
                    line = line.replace('REPORT DATE:','').replace('_','').strip()
                    data['report_date'] = line.strip()

                if 'GENE' in line and '/' in line and 'FAMILY' in line and 'RESULT' in line:
                    start_parsing = True
                    col1_end = line.find('RESULT') - 3
                    continue


                if start_parsing:
                    if 'THE DNA COMPANY' in line:
                        break

                    if line.strip():
                        key = line[0:col1_end].strip()
                        key = key.replace(' ','_')
                        data[key] =  line[col1_end:].strip()
    return data


def get_dataObj_LBR(pdf_path):
    pdf_path = pathlib.Path(pdf_path)
    with open(pdf_path, "rb") as f:
        pdf = pdftotext.PDF(f, raw=False)

    data = {}
    start_parsing = False
    for page_no,page in enumerate(pdf):
        lines = page.splitlines()
        start_parsing = False
        for idx,line in enumerate(lines):
            if page_no == 0:
                # print(line)
                if "Patient Name:" in line[:30]:
                    name_end = line.find("Home Phone")
                    curr_line_clean = line[:name_end].replace('Patient Name:','').strip()
                    data['name'] = curr_line_clean

                if "Date of Birth:" in line[:30]:
                    name_end = line.find("Work Phone")
                    curr_line_clean = line[:name_end].replace('Date of Birth:','').strip()
                    data['dob'] = curr_line_clean

                dos_lbl = "Date of Service:"
                if  dos_lbl in line:
                    start = line.find(dos_lbl)
                    dos_key = dos_lbl.lower().replace(' ','_').replace(':','')
                    data[dos_key] = line[start+len(dos_lbl):].strip()

            head_lbl1 = 'Test Name(s)'
            head_lbl2 = 'Result'
            if head_lbl1 in line and head_lbl2 in line:
                header_line = line
                start_parsing = True
                col1_end = header_line.find(head_lbl1) + len(head_lbl1)
                col2_end = header_line.find(head_lbl2) + len(head_lbl2)

            if not start_parsing:
                continue

            if  'HbA1C' in line[:20]:
                lbl = 'HbA1C'.lower()
                data[lbl] = line[len(lbl):col2_end].strip()

            if line[:col1_end].strip().startswith('eGFR'):
                lbl = 'eGFR'.lower()
                data[lbl] = line[col1_end:col2_end].strip()

            if line[:col1_end].strip().startswith('URATE'):
                lbl         = 'URATE'.lower()
                data[lbl]   = line[col1_end:col2_end].strip()

            if line[:col1_end].strip() == 'CHOLESTEROL':
                lbl = 'CHOLESTEROL'.lower()
                data[lbl] = line[col1_end:col2_end].strip()

            if line[:col1_end].strip().startswith('LDL CHOLESTEROL CALC'):
                lbl = 'LDL_CHOLESTEROL_CALCULATED'.lower()
                data[lbl] = line[col1_end:col2_end].strip()

            if 'ALANINE TRANSAMINASE' in line:
                lbl = 'ALANINE_TRANSAMINASE_ALT'.lower()
                data[lbl] = line[len(lbl)-4:col2_end].strip()

            if 'GAMMA' in line [:20]:
                lbl         = 'GAMMA_GLUTAMYLTRANSFERASE'.lower()
                data[lbl]   = line[len(lbl)-4:col2_end].strip()

            if 'GAMMA' in line [:20]:
                lbl = 'GAMMA_GLUTAMYLTRANSFERASE'.lower()
                data[lbl] = line[len(lbl)-4:col2_end].strip()

            if 'ASPARTATE' in line [:20]:
                lbl = 'ASPARTATE_TRANSAMINASE_AST'.lower()
                data[lbl] = line[len(lbl)-4:col2_end].strip()

            if 'TRIGLYCERIDES' in line[:20]:
                lbl = 'TRIGLYCERIDES'.lower()
                data[lbl] = line[len(lbl):col2_end].strip()


            if 'HDL CHOLESTEROL' in line[:16]:
                lbl = 'HDL_CHOLESTEROL'.lower()
                data[lbl] = line[len(lbl)+4:col2_end].strip()

            if 'CHOLESTEROL/HDL RATIO' in line[:25]:
                lbl = 'CHOLESTEROL_HDL_RATIO'.lower()
                data[lbl] = line[len(lbl):col2_end].strip()

            if line[:col1_end].strip().startswith('LDL'):
                if 'CHOLESTEROL' in lines[idx+1] and 'CALCULATED' in lines[idx+1]:
                    lbl = 'LDL_CHOLESTEROL_CALCULATED'.lower()
                    data[lbl] = line[len(lbl)-10:col2_end].strip()

            if 'TESTOSTERONE-FREE' in line[:25] or 'FREE TESTOSTERONE' in line[:col1_end]:
                lbl = 'TESTOSTERONE_FREE'.lower()
                data[lbl] = line[len(lbl):col2_end].strip()

            if 'DHEA-S' in line[:col1_end] or 'DHEAS' in line[:col1_end]:
                lbl = 'DHEA_S'.lower()
                data[lbl] = line[len(lbl):col2_end].strip()

            if 'INSULIN:FASTING' in line[:25]:
                lbl = 'INSULIN_FASTING'.lower()
                data[lbl] = line[len(lbl):col2_end].strip()

            if 'C-REACTIVE PROTEIN' in line[:25]:
                lbl = 'C_REACTIVE_PROTEIN'.lower()
                data[lbl] = line[len(lbl):col2_end].strip()

            if '25-HYDROXY VITAMIN D' in line[:25]:
                lbl = '25_HYDROXY_VITAMIN_D'.lower()
                data[lbl] = line[len(lbl):col2_end].strip()

            if line[:col1_end].strip().startswith('TC/HDL-C RATIO'):
                lbl = 'TC_HDL_C_RATIO'.lower()
                data[lbl] = line[len(lbl):col2_end].strip()

            if line[:col1_end].strip().startswith('HIGH SENSITIVITY CRP'):
                lbl = 'HIGH_SENSITIVITY_CRP'.lower()
                data[lbl] = line[len(lbl):col2_end].strip()

            if line[:col1_end].strip().startswith('VITAMIN B12'):
                lbl = 'VITAMIN_B12'.lower()
                data[lbl] = line[len(lbl):col2_end].strip()

            if line[:col1_end].strip().startswith('FERRITIN'):
                lbl = 'FERRITIN'.lower()
                data[lbl] = line[len(lbl):col2_end].strip()

            if line[:col1_end].strip().startswith('ALKALINE PHOSPHATASE'):
                lbl = 'ALKALINE_PHOSPHATASE'.lower()
                data[lbl] = line[len(lbl):col2_end].strip()

            if line[:col1_end].strip().startswith('TSH'):
                lbl = 'TSH'.lower()
                data[lbl] = line[len(lbl):col2_end].strip()

            if line[:col1_end].strip().startswith('GGT'):
                lbl = 'GGT'.lower()
                data[lbl] = line[len(lbl):col2_end].strip()

            if line[:col1_end].strip().startswith('AST'):
                lbl = 'AST'.lower()
                data[lbl] = line[len(lbl):col2_end].strip()

            if line[:col1_end].strip().startswith('ALT'):
                lbl = 'ALT'.lower()
                data[lbl] = line[len(lbl):col2_end].strip()

            if line[:col1_end].strip().startswith('HEMOGLOBIN A1c'):
                lbl = 'HEMOGLOBIN_A1c'.lower()
                data['hba1c'] = line[len(lbl):col2_end].strip()

            if line[:col1_end].strip() =='TESTOSTERONE':
                lbl = 'TESTOSTERONE'.lower()
                data[lbl] = line[len(lbl):col2_end].strip()

            if line[:col1_end].strip().startswith('ESTRADIOL'):
                lbl = 'ESTRADIOL'.lower()
                data[lbl] = line[len(lbl):col2_end].strip()

            if line[:col1_end].strip() == 'INSULIN':
                lbl = 'INSULIN'.lower()
                data[lbl] = line[len(lbl):col2_end].strip()

            if line[:col1_end].strip() == 'HEMOGLOBIN':
                lbl = 'HEMOGLOBIN'.lower()
                data[lbl] = line[len(lbl):col2_end].strip()

            if line[:col1_end].strip() == 'HOMOCYSTEINE':
                lbl = 'HOMOCYSTEINE'.lower()
                data[lbl] = line[len(lbl):col2_end].strip()

            if line[:col1_end].strip() == 'LP-PLA2 ACTIVITY':
                lbl = 'LP_PLA2_ACTIVITY'.lower()
                data[lbl] = line[len(lbl):col2_end].strip()

            if line[:col1_end].strip().startswith('GLUCOSE-FASTING'):
                lbl = 'GLUCOSE-FASTING'.lower()
                data[lbl] = line[len(lbl):col2_end].strip()

    return data