keysMappingLBR = {
    "fname": "First Name",
    "lname": "Last Name",
    "dob": "Date of Birth",
    "vitamin_b12": "Vitamin B12",
    "hba1c": "HBAIC %",
    "dhea_s": "DHEA",
    "hemoglobin": "Hemoglobin",
    "testosterone_free": "Free Testosterone",
    "insulin_fasting": "Insulin Fasting",
    "high_sensitivity_crp": "hsCRP",
    "ferritin": "Ferritin",
    "25_hydroxy_vitamin_d": "25 H Vit D",
    "homocysteine": "Homocysteine",
    "lp_pla2_activity": "LpPLA2",

    "egfr": "EGFR",#--new
    "urate": None,
    "cholesterol": None,
    "triglycerides": 'Lipid - TG',
    "hdl_cholesterol": 'Lipid - HDL',
    "ldl_cholesterol_calculated": 'Lipid - LDL',
    "tc_hdl_c_ratio": "TC HDL C Ratio", #new
    "alkaline_phosphatase": "ALK", #(new)
    "ggt":"GGT", #NEW ,
    "ast":"AST", #NEW ,
    "alt":"ALT", #NEW ,
    "tsh":"TSH", #NEW ,
    "testosterone": None,
    "estradiol": "Estradiol",
    "insulin": "Insulin Fasting",
    "alanine_transaminase_alt": "ALT",
    "gamma_glutamyltransferase": "TSH",
    "aspartate_transaminase_ast": "AST",
    "cholesterol_hdl_ratio": "TC HDL C Ratio",
    "c_reactive_protein": "hsCRP",
    "glucose-fasting": None,
}


keysMappingHPR = {
    "fname": "First Name",
    "lname": "Last Name",
    "CYP17A1": [
        "CYP17A1\r\nRs743572 A/G",
        "CYP17A1\r\nRs743572 G/A"
    ],

    "SHBG_(m)": None,
    "SHBG_(RS6258)": None,
    "AR": None,
    "SRD5A2": "SRD5A2\r\nRs523349 C/G",
    "CYP19A1": "CYP19A1\r\nRs 10046 C/T",

    "UGT2B17": [
        "UGT2B17\nCNV 0/2",
        "UGT2B17\r\nCNV 1/2"
    ],

    "UGT2B15": "UGT2B15\r\nRs1902023 T/G",
    "CYP3A4": "CYP3A4\r\nRs2740574 A/G",
}


keysMappingGPR = {
    "fname": "First Name",
    "lname": "Last Name",

    "COMT_Rs4680": [
        "MB COMT A/G Rs4680",
		"COMT\r\nRs4680 G/A",
		"COMT Rs4680 A/G",
    ],
    "DRD2_Rs1800497": [
		"MB DRD2 Rs1800497",
		"DRD2 Rs1800497 C/T",
		"(DRD2)Rs1800497 G/A",
    ],
    "ADRA2B_Indel": [
		"MB ADRA2B Indel",
		"(ADRA2B)\r\nIndel I/D",
    ],

    "5-HTTLPR_Indel": [
		"(5-HTTLPR)\r\nIndel L/S",
		"MB 5-HTTLPR Indel L/S",
    ],

    "BDNF_Rs6265": [
		"(BDNF)\r\nRs6265 G/A",
		"MB BDNF Rs6265 G/A",
		"BDNF Rs6265 A/G"
    ],
    "9P21_Rs10757278": [
		"CV 9P21 Rs10757278 G/A",
		"9P21\r\nRs10757278\r\nRs10757274 A/G", # confusion
		"CV 9P21 Rs10757278 G/A",
    ],
    "9P21_Rs10757274": [
		"CV 9P21\nRs10757274",
		"9P21\r\nRs10757278\r\nRs10757274 A/G",

    ],
    "APOE_Rs429358_Rs7412": [
		"CV APOE\r Rs429358 \rRs7412  ",
		"(APOE)\nRs7412 / Rs429358 E2,E3,E4",
    ],
    "NOS3_Rs1799983": [
        "CV NOS3 Rs1799983 G/T",
		"(NOS3)\r\nRs1799983 G/T",
    ],
    "ACE_Rs4343": [
		"ACE\nRs4343 A/G",
		"(ACE)\r\nRs4343 A/G",
		"(ACE)\r\nRs4343 A/G",
    ],
    "SLCO1B1_Rs4149056": [
		"SLCO1B1 Rs4149056 T/",
		"CV SLCO1B1\nRs4149056 C/T",
		"(SLCO1B1)\r\nRs4149056 T/C",
    ],
    "FUT2_Rs601338": [
		"M FUT2 Rs601338 A/G",
		"(FUT2)\r\nRs601338 A/G",
		"(FUT2)\r\nRs601338 A/G",
    ],
    "MTHFR_Rs1801133": [
		"M MTHFR\nRs1801133 C/T",
		"(MTHFR)\r\nRs1801133 T/C",
		"MTHFR Rs1801133 T/",
    ],
    "SHMT1_Rs1979277": [
		"M SHMT1\nRs1979277 A/G",
		"(SHMT1)\r\nRs1979277 G/A",

    ],
    "MTR_Rs1805087": [
		"(MTR)\r\nRs1805087 A/G",
		"M MTR \nRs1805087 A/G",
    ],
    "MTRR_Rs1801394": [
		"MTRR\nRs1801394 A/G",
		"MTRR Rs1801394 A/G",
		"(MTRR)\r\nRs1801394 A/G",
    ],
    "MTHFR_Rs1801131": [
		"MTHFR Rs1801131 A/ ",
		"MTHFR\nRs1801131 A/C",

    ],
    "FTO_Rs9939609": [
		"(FTO)\r\nRs9939609 T/A",
		"FTO Rs9939609 A/T",
		"FTO\nRs9939609 T/A",
    ],
    "MC4R_Rs17782313": [
		"MC4R\r Rs17782313 T/C",
		"MC4R\r Rs17782313 T/C",
		"MC4R Rs17782313 C/T",
    ],
    "APOA2_Rs5082": [
		"APOA2 Rs5082APO  A/G",
		"(APOA2)\r\nRs5082 A/G",
    ],
    "UCP1_1800592": [
		"UCP1 Rs1800592 A/G",
		"(UCP1)\r\nRs1800592 A/G",
		"UCP1\r1800592 A/G",

    ],
    "TCF7L2_Rs12255372": [
		"TCF7L2 Rs12255372 G/T",
		"TCF7L2\nRs12255372 G/T",
		"(TF7L2)\r\nRs12255372 G/T",
    ],
    "AMY1_Rs4244372": [
		"(AMY1)\r\nRs4244372 A/T",
		"(AMY1)\r\nRs4244372 A/T",
    ],
    "MCM6_Rs4988235": [
		"MCM6\nRs4988235 A/G",
		"(MCM6)\r\nRs4988235 A/G",
		"(MCM6)\r\nRs4988235 A/G",
    ],
    "BCMO1_Rs11645428": [
		"BCMO1\nRs11645428 A/G",
		"(BCMO1)\r\nRs11645428 A/G",
    ],
    "SLC23A1_Rs33972313": [
		"(SLC23A1)\r\nRs33972313 G/A",
		"SLC23A1 Rs33972313 G/A",
    ],
    "CYP2R1_Rs10741657": [
		"CYP2R1\nRs10741657 A/G",
		"(CYP2R1)\r\nRs10741657 A/G",

    ],
    "GC_Rs4588": [
		"GC\n Rs4588 C/A",
		"(GC)\r\nRs4588 A/C",
    ],
    "CYP1A2_Rs762551": [
		"(CYP1A2)\r\nRs762551 C/A",
		"CYP1A2 Rs762551 A/C",
		"CYP1A2 Rs762551 C/A",
    ],
    "GSTT1_CNV": [
		"GSTT1\nCNV 0-2",
		"(GSTT1)\r\nCNV 1/2",
    ],
    "GSTM1_CNV": [
		"(GSTM1)\r\nCNV 1/2",
		"GSTM1\nCNV 0-2",
    ],
    "GSTP1_Rs1695": [
		"GSTP1\nRs1695 A/G",
		"(GSTP1)\r\nRs1695 A/G",
		"GSTP1 Rs1695 A/G",
    ],
    "SOD2_Rs4880": [
		"SOD2 Rs4880 C/T",
		"SOD2\r\nRs4880 C/T",
		"(SOD2)\r\nRs4880 C/T",
    ],
}
