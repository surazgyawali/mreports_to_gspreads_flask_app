
function get_error_content(error_list) {
    text = ''
    error_list.forEach(element => {
            text += 'Filename: <b>' + element.filename + '</b>'
            text += '<br>'
            text += 'Error: <b class="text-danger">' + element.error + '</b>'
            text += '<br>'
    });
    return text
}


function get_success_content(successful_list) {
    text = ''
    successful_list.forEach(element => {
        element = element.trim()
        if (element.length > 1) {
            text += '<b class="text-success">' + element + '</b>'
            text += '<br>'
        }
    });
    return text
}


function show_success_response(successful_list, errors_list) {

    titleText = '<span class="text-primary"><b>The uploaded documents were successfully processed<br>Summary: </b></span>'

    $('#modalLabel')[0].innerHTML = titleText

    if (errors_list.length > 0) {
        $('#errors')[0].innerHTML = get_error_content(errors_list)
        $('#success')[0].innerHTML = get_success_content(successful_list)
    } else{
        $('.errors').hide()
        $('.successful').hide()
        $('#success').show()
        $('#success')[0].innerHTML = '<b class="text-success">All the files were successfully updated to sheets.</b>'
    }

    $('#fmodal').modal('show')

}

$(document).ready(function () {

    $("#inputForm").submit(function (event) {
        event.preventDefault();
        var formEl = $(this)
        var form = formEl[0]; // You need to use standard javascript object here
        var formData = new FormData(form);
        $.ajax({
            xhr: function () {
                var xhr = new window.XMLHttpRequest();

                xhr.upload.addEventListener("progress", function (evt) {
                    if (evt.lengthComputable) {
                        var percentComplete = evt.loaded / evt.total;
                        percentComplete = parseInt(percentComplete * 100);
                        $(".progress-bar").width(percentComplete + "%");
                        $("#uploading").text("Uploading: " + percentComplete + "%")
                        if (percentComplete === 100) {
                            $("#uploading").text("")
                        }
                    }
                }, false);
                return xhr;
            },
            beforeSend: function () {

                $("#spinner").show()

            },
            complete: function () {
                $("#spinner").hide();
                $(".progress-bar").hide()
            },
            error: function (xhr, error, thrownError) {
                $("#spinner").hide();
                $(".progress-bar").hide()
                if (xhr.status && xhr.status == 400) {
                    alert(xhr.responseText);
                } else {
                    console.log(xhr, error, thrownError)
                    // alert("Your file was rejected, It seems to be not supported, try a different file.");
                }
            },
            url: "/",
            type: "POST",
            dataType: 'json', //expected data type
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST',
        }).done(function (data) {
            var input = document.querySelector('input[type="file"]')
            input.outerHTML=input.outerHTML

            $("#spinner").hide();
            $(".progress-bar").hide()
            var type = data['type']
            if (type == 'info' || type == 'error') {
                var message = data["message"]
                alert(message)
                return
            } else {
                var successful_list = data['successful']
                var error_list = data['errors']

                window.successful_list = successful_list
                window.error_list = error_list

                show_success_response(successful_list, error_list)
                return

            }
        });
    });

});