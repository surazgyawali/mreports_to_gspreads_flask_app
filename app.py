import os
import uuid
import glob
import secrets
import pathlib

import flask

from werkzeug.utils import secure_filename


UPLOAD_FOLDER = "uploaded"
OUTPUT_FOLDER = "output"
ALLOWED_EXTENSIONS = set(["pdf"])

secret_key = secrets.token_urlsafe(16)
app = flask.Flask(__name__)

from pdf.get_data import get_dataObj_GPR, get_dataObj_HPR, get_dataObj_LBR
from pdf.update_sheets import update_all


app.secret_key = secret_key
app.config["UPLOAD_FOLDER"] = UPLOAD_FOLDER
app.config["OUTPUT_FOLDER"] = OUTPUT_FOLDER
app.config["MAX_CONTENT_LENGTH"] = 16 * 1024 * 1024


def allowed_file(filename):
    return "." in filename and filename.rsplit(".", 1)[1].lower() in ALLOWED_EXTENSIONS

@app.errorhandler(413)
def too_large(e):
    return "File is too large", 413

@app.route("/")
def upload_form():
    return flask.render_template("index.djhtml")


@app.route("/", methods=["POST"])
def upload_file():
    if flask.request.method == "POST":
        # check if the post request has the files part
        if "files[]" not in flask.request.files:
            return flask.jsonify(
                {
                    "type": "info",
                    "message": "Uh oh, seems like the file is missing or not proper, Please try again maybe with a different file.",
                }
            )
        file_type = flask.request.form.get("file_type")
        print(file_type)
        files = flask.request.files.getlist("files[]")

        upload_folder = app.config["UPLOAD_FOLDER"]
        if not os.path.exists(upload_folder):
            os.makedirs(upload_folder)

        curr_files = glob.glob(f"{upload_folder}/*")
        for f in curr_files:
            os.remove(f)

        filename = None
        errors = []
        succedeed = []
        for file in files:
            if file and allowed_file(file.filename):
                filename = secure_filename(file.filename)
                file.save(os.path.join(upload_folder, filename))

            if filename is None:
                errors.append(
                    {
                        "type": "error",
                        "filename": file.filename,
                        "message": "Seems like  this is a invalid PDF file.",
                    }
                )
                continue

            read_path = pathlib.Path(os.path.join(upload_folder, filename))

            try:

                if file_type == 'gpr':
                    dataObj = get_dataObj_GPR(read_path)
                elif file_type == 'hpr':
                    dataObj = get_dataObj_HPR(read_path)
                elif file_type== 'lbr':
                    dataObj = get_dataObj_LBR(read_path)
                else:
                    dataObj = None

                if not dataObj:
                    errors.append(
                        {
                            "type": "error",
                            "filename": file.filename,
                            "message": "Seems like  this is a invalid PDF file, try with different.",
                        }
                    )
                    continue

                update_all(dataObj)
                succedeed.append(filename)
            except Exception as e:
                print(str(e))
                errors.append(
                    {   "filename": file.filename or "",
                        "type": "error",
                        "message": f"Something went wrong from our side, please try again later. Reason: {e}",
                    }
                )

        return (
            flask.jsonify(
                {
                    "type": "success",
                    "message": f"Successfully updated files.",
                    "successful": succedeed,
                    "errors": errors,
                }
            ),
            200,
        )


@app.after_request
def add_header(r):
    """
    Add headers to both force latest IE rendering engine or Chrome Frame,
    and also to cache the rendered page for 10 minutes.
    """
    r.headers["Cache-Control"] = "no-cache, no-store, must-revalidate"
    r.headers["Pragma"] = "no-cache"
    r.headers["Expires"] = "0"
    r.headers["Cache-Control"] = "public, max-age=0"
    return r


if __name__ == "__main__":
    app.debug = True
    app.run(host="0.0.0.0", port=8848)
